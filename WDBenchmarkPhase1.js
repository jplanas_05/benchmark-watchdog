/////////////////////////////////////////////////////////////////////////////////////////////////
//	AgentSelly Benchmark v.1.0
//	
//  Usage: phantomjs WDBenchmarkPhase1.js <URL>
//
//	Argument (base 0) 1: URL page, for example http://www.agentselly.ch
//	Usage (in command line): phantomjs WDBenchmarkPhase1.js https://www.agentselly.ch
/////////////////////////////////////////////////////////////////////////////////////////////////

//Disintegration error handling (comment line per line) cuz is complicated to debug throw (doesnt respond properly). Try - Catch!!!!. Is in the API but.... Temporary comment.

"use strict"; // Strict compatible mode according to standard

// Prototype function to format dates to ISO standard
if (!Date.prototype.toISOString)
{
    Date.prototype.toISOString = function ()
	{
        function pad(n)	{	return n < 10 ? '0' + n : n; }
        function ms(n) 	{	return n < 10 ? '00'+ n : n < 100 ? '0' + n : n; }
		// output date formatted
        return this.getFullYear() + '-' + pad(this.getMonth() + 1) + '-' + pad(this.getDate()) + 'T' +pad(this.getHours()) + ':' +pad(this.getMinutes()) + ':' +(this.getSeconds()) + '.' + ms(this.getMilliseconds()) + 'Z';
    }
}

// Global Variables withour HAR (single). In other args i can insert the result HAR file for debug manually in HAR Viewer. Just to test.
// Reminder: Better use defaults
// Reminder: Better make an JSON object.
var request, startReply, endreply;
var useOpen = true;
var id;
var pageref;
var title;;
var onLoadTiming;
var excludeCSSInRequest;
var IncludesRequiredScripts;
var IncludesRequiredJSON;
var ErrorsInConsole;
var timeoutErrors;
var startedDateTime;
var url;
var time;
var headers;
var bodySize;
var	statusCode;
var statusText;
var mimeType;
var receive;

var page = require('webpage').create(), system = require('system');
var fs = require('fs');

page.resources = [];

// General errors for all execution process (global, not syntax!!!). Displayed in custom header for output.
phantom.onError = function(msg, trace)
{
	//better in black list after or include exports (enum for error codes???)
	// WARNING: Hardcoded CODE 4
	
	//temporary to show some erros (this doesnt work very well seems)
	
	var videojs = msg.indexOf('CODE:4 MEDIA_ERR_SRC_NOT_SUPPORTED'); //headless mode so its not load media by the moment (other solutions availables)
	//if (videojs === -1) { console.log(JSON.stringify(trace)); }
	
	var msgStack = ['ERROR: ' + msg];

	if (trace && trace.length)
	{
		msgStack.push('TRACE:');
		trace.forEach(function(t)
		{
			msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
		});
	}

	console.log(msgStack.join('\n'));
	phantom.exit(1);
};

if (system.args.length !== 2) //first the script to execute and second the <URL page>
{
	page.address = "None: Usage: phantomjs WDBenchmarkPhase1.js <URL>";
	useOpen = false;
}
else page.address = system.args[1];

function defHARAndExtract(address, title, startTime, resources)
{
	
	resources.forEach(function (res)
	{
		/*
        request = res.request;
		startReply = res.startReply;
		endReply = res.endReply;
		
		//using C-JS-like spelling
		
		/*
		request 	= 	(request) 		? "OK" : "Failed";
		startReply 	=	(startReply) 	? "OK" : "Failed";
		endReply 	=	(endReply) 		? "OK" : "Failed";
		
		pageref: page.address;
		id = page.address;
		title = title;
		onLoadTiming =  page.endTime - startTime;
		
		if (request)
		{
			startedDateTime = request.time.toISOString();
			url = request.url;
			if (endReply)
			{
				time = endReply.time - request.time;
			}
			else if (request.headers)
			{
				headers: request.headers;
			}
			else if (startReply)
			{
				waitPage = startReply.time - request.time;
			}
		}
		else if (startReply)
		{
			bodySize =  startReply.bodySize;
		}
		else if (endReply)
		{
			statusCode = endReply.status;
			statusText = endReply.statusText;
			mimeType: (endReply.contentType;
			if (startReply)
			{
				receive: endReply.time - startReply.time;
			}
		}
		*/
		
		
    });
}

page.customHeaders =
{
	"WD-request": 'testPhase1_' + request;
	"WD-startReply": "testPhase1_" + startReply;
	"WD-endReply": "testPhase1_" + endReply;
	"WD-Test-id": "testPhase1_" + startedDateTime;
	
	// Errors
	"WD-Error-ErrorsInConsole" : "testPhase1_" + ErrorsInConsole;
	"WD-Error-Timeout" : "testPhase1_" + timeoutErrors;
	
	// Info
	"WD-Info-Excludes" : "testPhase1_" + excludeCSSInRequest;
	"WD-Info-IncludesRequiredScripts" : "testPhase1_" + IncludesRequiredScripts;
	"WD-Info-IncludesRequiredJSON-like" : "testPhase1_" + IncludesRequiredJSON;
};

//page.onInitialized = function() { page.customHeaders = {}; };

//page.onLoadStarted = function () { page.startTime = new Date(); };

page.onResourceRequested = function (req)
{
	
	console.log("enter requested");
	
	page.resources[req.id] = { request: req, startReply: null, endReply: null };
	
	// Reminder: I think better make a "white" list (all in regular expressions)
	// By the moment in parts for better understanding
	
	// Load page with no css libs located by content-type (optimization request)
	if ((/http|https:\/\/.+?\.css/gi).test(requestData['url']) || requestData.headers['Content-Type'] == 'text/css') { excludeCSSInRequest = requestData['url']; }
	
	//display info only scripts (use two estandards) located by content-type (required cuz its important!!)
	if ((/http|https:\/\/.+?\.js/gi).test(requestData['url']) || ( requestData.headers['Content-Type'] === 'text/javascript' || requestData.headers['Content-Type'] === 'application/javascript')) {
		IncludesRequired = requestData['url'];
	}
	
	//display info only json|jsonp located by content-type (required cuz its important!!, -if any-)
	if ((/http\https:\/\/.+?\.json|jsonp/gi).test(requestData['url']) || (requestData.headers['Content-Type'] === 'application/json' || requestData.headers['Content-Type'] === 'application/jsonp')) {
		IncludesRequiredJSON = requestData['url'];
	}
};


page.onResourceReceived = function (res)
{
	if (res.stage === 'start')	{ page.resources[res.id].startReply = res;	}
	if (res.stage === 'end') 	{ page.resources[res.id].endReply = res;	}
};
	
	
//page.onConsoleMessage = function( message, line, srcId) { ErrorsInConsole = 'Page has errors in console: ' + msg + ' (line: ' + (line || '?') + ', id: ' + srcId + ')'; };

//page.settings.XSSAuditingEnabled = false;
//page.settings.javascriptCanOpenWindows = false;
//page.settings.javascriptCanCloseWindows = false;
//page.settings.javascriptEnabled = true;
//page.settings.loadImages = false;
//page.settings.localToRemoteUrlAccessEnabled = false;
//page.settings.webSecurityEnabled = true;
//page.settings.resourceTimeout = 5000; // in milliseconds, 5 seconds max
// using AgentSpecialSellyWD user agent to identify ourself as WatchDog. Get user agent data in any page for identify us (and output data)
// WARNING: Harcoded user agent
//page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/601.1.54 AppleWebKit/538.1 (KHTML, like Gecko) Chrome/37.0.2062.120 Chrome/66.0.3359.181 Safari/538.1 (Windows NT 6.2; WOW64) PhantomJS/2.1.1 AgentSpecialSellyWD';

//page.onResourceTimeout = function(e) { timeoutErrors = "Code: " + e.errorCode + "Definition: " + e.errorString + "URL: " + e.URL; };

//page.onResourceReceived = function(response)
//{
	//id = response.id;
	/*
	url : the URL of the requested resource
	time : Date object containing the date of the response
	headers : list of http headers
	bodySize : size of the received content decompressed (entire content or chunk content)
	contentType : the content type if specified
	redirectURL : if there is a redirection, the redirected URL
	stage : “start”, “end” (FIXME: other value for intermediate chunk?)
	status : http status code. ex: 200
	statusText : http status text. ex: OK
	*/
	
	//console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
//};

// only need for page.endTime, document.title and defHARAndExtract creation
if (useOpen)
{
	console.log(page.address);
	
	page.open(page.address, function(status)
	{
	
		console.log("enter");
		
		/*
		var info;
		
		console.log(page.address);
		console.log(status);
		
		if (status === 'success')
		{
			page.title = page.evaluate(function () { return document.title; });
			page.endTime = new Date();
			info = defHARAndExtract(page.address, page.title, page.startTime, page.resources);
			//fs.write('json_spec.json_' + startedDateTime, JSON.stringify(info, undefined, 4), 'w');
		}
		*/
	});
}

// Always exit with no problem
phantom.exit(1);
